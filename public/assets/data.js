function newData(){
	$('.list').removeClass('fadeInDown')
	$('.list').addClass('undisplay')

	$('.form').removeClass('undisplay')
	$('.form').addClass('fadeInDown')

	$('.report').removeClass('fadeInDown')
	$('.report').addClass('undisplay')

	$('[store]').val('');
}

function backToList(){
	$('.list').removeClass('undisplay')
	$('.list').addClass('fadeInDown')

	$('.form').addClass('undisplay')
	$('.form').removeClass('fadeInDown')

	$('.report').addClass('undisplay')
	$('.report').removeClass('fadeInDown')
}

function showReport(ini){
	$('.list').removeClass('fadeInDown')
	$('.list').addClass('undisplay')

	$('.form').removeClass('fadeInDown')
	$('.form').addClass('undisplay')

	$('.report').removeClass('undisplay')
	$('.report').addClass('fadeInDown')

	spinnerStart();

	let id = $(ini).attr('data-id');

	$.ajax({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		url: url+'/getdata',
		method: 'POST',
		data:{id:id},
		success: function(response){

			$.each(response[0],function(i,v){
				$('[store="'+i+'"]').val(v).change();

				if ($('[store="'+i+'"]').hasClass('select2')){
					$('[store="'+i+'"]').val(v)
				}
			})

			$('#peserta_nama').text(response[0].nama)
			$('#peserta_email').text(response[0].email)
			var x = response[0].x
			var y = response[0].y
			var z = response[0].z
			var w = response[0].w
			
			var nilai_intelegensi = (40* parseInt(x) / 100) + ((60*parseInt(y) / 100) / 2)
			var nilai_numerical = (30* parseInt(z) / 100) + ((70*parseInt(w) / 100) /2)

			$('#aspekintelegensi td').html('')
			$('#aspeknumerical td').html('')
			$('#aspekintelegensi td:nth-child(1)').html('Aspek Intelegensi')
			$('#aspeknumerical td:nth-child(1)').html('Aspek Numerical Ability')

			
			if (nilai_intelegensi<=5.62){
				$('#aspekintelegensi td:nth-child(2)').html('✔')
			}
			else if (nilai_intelegensi<=9.24){
				$('#aspekintelegensi td:nth-child(3)').html('✔')
			}
			else if (nilai_intelegensi<=12.86){
				$('#aspekintelegensi td:nth-child(4)').html('✔')
			}
			else if (nilai_intelegensi<=16.48){
				$('#aspekintelegensi td:nth-child(5)').html('✔')
			}
			else if (nilai_intelegensi<=20.1){
				$('#aspekintelegensi td:nth-child(6)').html('✔')
			}

			console.log(nilai_intelegensi)
			console.log(nilai_numerical)

			if (nilai_numerical<=3.59){
				$('#aspeknumerical td:nth-child(2)').html('✔')
			}
			else if (nilai_numerical<=5.18){
				$('#aspeknumerical td:nth-child(3)').html('✔')
			}
			else if (nilai_numerical<=6.77){
				$('#aspeknumerical td:nth-child(4)').html('✔')
			}
			else if (nilai_numerical<=8.36){
				$('#aspeknumerical td:nth-child(5)').html('✔')
			}
			else if (nilai_numerical<=9.95){
				$('#aspeknumerical td:nth-child(6)').html('✔')
			}

		}
	})
	spinnerEnd();

}

function editData(ini){
	$('.list').removeClass('fadeInDown')
	$('.list').addClass('undisplay')

	$('.form').removeClass('undisplay')
	$('.form').addClass('fadeInDown')

	$('.report').removeClass('fadeInDown')
	$('.report').addClass('undisplay')

	spinnerStart();

	let id = $(ini).attr('data-id');

	$.ajax({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		url: url+'/getdata',
		method: 'POST',
		data:{id:id},
		success: function(response){

			$.each(response[0],function(i,v){
				$('[store="'+i+'"]').val(v).change();

				if ($('[store="'+i+'"]').hasClass('select2')){
					$('[store="'+i+'"]').val(v)
				}
			})
		}
	})
	spinnerEnd();

}

function goSave(url){

	spinnerStart();

	let method = 'POST';
	let value='';
	let datas = {};
	let id = $('[store="id"]').val();
	let url2 = url;

	if (id != ''){
		url = url+'/'+id;
		method = 'PATCH';
		datas['id'] = id;
	}

	$('[store]').not('[store="id"]').each(function(i,v){
		value = $(v).val();
		if ($(v).hasClass('datepicker')){
			datas[$(v).attr('store')] = moment(value).format('yyyy-M-D')
		}
		else if ($(v).hasClass('numberFormat')){
			datas[$(v).attr('store')] = value.replace(',','')
		}
		else{
			datas[$(v).attr('store')] = value;
		}
	})

	console.log(datas,method)

// spinnerEnd()
// return false;


$.ajax({
	headers: {
		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	},
	url: url,
	method: method,
	data:datas,
	success: function(response){

		console.log(response)
		notify('top', 'center', 'success', 'inverse', 'animated fadeInDown', 'animated fadeOutDown' , 'bg-success','berhasil','simpan data');

		$('[store]').not('[store="id"]').each(function(i,v){
			$(v).removeClass('is-invalid');
			$(v).next('.invalid-feedback').addClass('invalid-feedback');
		});

		backToList();
		dtMaster(url2,dataColum);
		spinnerEnd();

	},
	error: function(response){
		let errors = response.responseJSON.errors;
		let urutan = 0;
		let focus = '';

		$.each(errors,function(i,v){
			urutan=urutan+1;
			if (urutan==1){
				index_response = i;
				info_response = v[0];
			}
		})

		$('[store]').not('[store="id"]').each(function(i,v){
			$(v).removeClass('is-invalid');
			$(v).next('.invalid-feedback').addClass('invalid-feedback');

		});

		urutan = 0;
		$.each(errors,function(i,v){
			urutan=urutan+1;
			$('[store="'+i+'"]').addClass('is-invalid');
					// $('[store="'+i+'"]').next().text(v);
					if (urutan == 1){
						focus = i;
					}
				})

		notify('top', 'center', 'danger', 'inverse', 'animated fadeInDown', 'animated fadeOutDown' , 'bg-danger',index_response,info_response);

		spinnerEnd();
		$('[store="'+focus+'"]').focus();
	}
})
}

function deleteData(ini){

	let ids=[];
	ids.push($(ini).attr('data-id'));

	Swal.fire({
		title: 'Hapus Data',
		text: 'data tidak dapat dikembalikan !',
		icon: 'warning',
		showCancelButton: true,
		confirmButtonText: 'Tidak',
		cancelButtonText: 'Ya',
		customClass: {
			confirmButton: 'btn btn-primary mr-2',
			cancelButton: 'btn btn-danger'
		},
		buttonsStyling: false
	}).then((result) => {
		if (!result.isConfirmed) {

			spinnerStart();
			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				url: url+'/ids',
				method: 'delete',
				data: {ids:ids},
				success: function(response){
					notify('top', 'center', 'success', 'inverse', 'animated fadeInDown', 'animated fadeOutDown' , 'bg-success','berhasil','hapus data');

					dtMaster(url,dataColum);

				}
			})
			spinnerEnd();
		}
	});

}