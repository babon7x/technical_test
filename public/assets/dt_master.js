
function dtMaster(url,dataColum){
	$('#datatable').dataTable().fnDestroy();
	$('#datatable').dataTable({
		"dom":'<"row undisplay"<"col-md-6"B>><"row"<"col-md-4"l><"col-md-4 periode"><"col-md-4 text-right"f>>tr<"row justify-content-center"<"col-md-12"p>>',
		// "order": [[ 1, "desc" ]],
		"columnDefs": 
		[
		{
			targets: [0],
			width: 25,
			orderable: false,
			searchable: false
		}
		],

		"language": {
			"lengthMenu": '<select class="form-control-sm">'+
			'<option value="10">10</option>'+
			'<option value="20">20</option>'+
			'<option value="30">30</option>'+
			'<option value="40">40</option>'+
			'<option value="50">50</option>'+
			'<option value="-1">All</option>'+
			'</select> data',
			"processing":
			'<i class="fa fa-cog fa-spin"></i>',
			"emptyTable":"Data Kosong",
			"searchPlaceholder": "pencarian",
			"infoEmpty":"tidak ada yang ditampilkan",
			"search":"",
			"zeroRecords":"Pencarian Kosong",
			
		},
		processing: true,
		serverSide: true,
		ajax: {
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			url: url+'/list',
			method: 'post',
			data:{}
		},
		columns:dataColum,
		"fnDrawCallback": function( oSettings ) {dtAjax_setting()},
		'fnCreatedRow': function (nRow, aData, iDataIndex) {
			$(nRow).attr('rowid', aData.id);
			$(nRow).addClass('cursor-pointer');
		},
	})
}

function dtAjax_setting(){
	setTimeout(function() { 
		$('ul.pagination').addClass('justify-content-center')
			// $('tbody tr').hover().css('cursor','pointer');
		}, 0);
}

$('#datatable').on('dblclick', 'tbody tr', function(e) {
	let rowid=$(this).attr('rowid');
	$('#rowid'+rowid).show();
})