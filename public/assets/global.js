// toggle sidebar
$('#toggle-sidebar').on('click',function(){
	$(this).find($(".fas")).toggleClass('fa-bars').toggleClass('fa-times');
	if ($(this).hasClass('show-sidebar')){
		$(this).addClass('close-sidebar');
		$(this).removeClass('show-sidebar');
		$('.main-sidebar').css('left','-350px');
		$('.toggle-sidebar').css('padding-left','0px');
	}
	else if ($(this).hasClass('close-sidebar')){
		$(this).addClass('show-sidebar');
		$(this).removeClass('close-sidebar');
		$('.main-sidebar').css('left','0px');
		$('.toggle-sidebar').css('padding-left','260px');
	}
})

$('#toggle-sidebar2').on('click',function(){
	$('#toggle-sidebar').click();
})

// toggle sidebar

// ctrl+space
var map = {17: false, 32: false};
$(document).keydown(function(e) {
	if (e.keyCode in map) {
		map[e.keyCode] = true;
		if (map[17] && map[32]) {
			$('#toggle-sidebar').click();
		}
	}
}).keyup(function(e) {
	if (e.keyCode in map) {
		map[e.keyCode] = false;
	}
});
// ctrl+space


function addCommas(nStr) {
	nStr += '';
	var x = nStr.split('.');
	var x1 = x[0];
	var x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + ',' + '$2');
	}
	return x1 + x2;
}

function nextArray(arrayData,arrayCurrentData){
	var next = arrayData[($.inArray(arrayCurrentData, arrayData) + 1) % arrayData.length];
	return next;
}

function prevArray(arrayData,arrayCurrentData){
	var prev = arrayData[($.inArray(arrayCurrentData, arrayData) - 1 + arrayData.length) % arrayData.length];
	return prev;
}

function spinnerStart(){
	$("#app").append(
		'<div class="spinner-body">'+
		'<div class="spinner">'+
		
		'<div class="card" >'+
		'<div class="card-body" >'+
		
		'<div class="la-ball-clip-rotate-pulse" style="color:var(--primary);">'+
		'<div></div>'+
		'<div></div>'+
		'</div>'+

		'</div>'+
		'</div>'+
		
		'</div>'+
		'</div>'
		);
}

function spinnerEnd(){
	$('.spinner-body').remove();
}

function addSidebarActive(menu_id){
	$('.nav-item[menu-id="'+menu_id+'"]').addClass('active')
	$('.nav-item[menu-id="'+menu_id+'"]').parent().addClass('show')
}

function clearModal(){
	$('[store]').val('')
} 

function downloadExcel(){
	$('.buttons-excel').click()
}

$(document).mouseup(function(e) 
{
	setpositionmenu(e);
	var container = $(".rowid");
	if (!container.is(e.target) && container.has(e.target).length === 0) 
	{
		container.hide();
	}
});

function setpositionmenu(e) {
	var bodyOffsets = document.body.getBoundingClientRect();
	tempX = e.pageX - bodyOffsets.left;
	tempY = e.pageY;
	var p = $('.table').scrollLeft();

	$(".rowid").css({ 'left': tempX-170 + p });

}
