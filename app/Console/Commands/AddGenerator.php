<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class AddGenerator extends Command
{
    protected $signature = 'generator:create';

    protected $description = 'create generator file';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {   
         $path = public_path() . '/../add/Controllers/Functions/Generator.php';
         File::put($path,'<?php');
    }
}

