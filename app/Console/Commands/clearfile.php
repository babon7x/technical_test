<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\Storage;

class clearfile extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clear:file';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'delete all file for clear project';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
       $file = new Filesystem;
       $file->cleanDirectory('storage/app/level');
       $file->cleanDirectory('storage/app/user');
   }
}
