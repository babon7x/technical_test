<h3>Created By babon7x@gmail.com</h3>

## Requirements

- php 7.3.
- mysql.
- composer.
- browser.

## Run This after u clone

- setting env file.
- php artisan db:create.
- composer update / composer install.
- composer dump-autoload.
- php artisan optimize.
- php artisan migrate:refresh --seed.


