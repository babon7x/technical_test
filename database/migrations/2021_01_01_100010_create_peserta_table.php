<?php 
use Illuminate\Support\Facades\Schema; 
use Illuminate\Database\Schema\Blueprint; 
use Illuminate\Database\Migrations\Migration; 

class CreatePesertaTable extends Migration 
{
public function up(){
Schema::create('peserta', function (Blueprint $table) {
$table->bigIncrements('id');
 $table->String('nama')->nullable();
 $table->String('email')->nullable();
 $table->Decimal('x')->nullable();
 $table->Decimal('y')->nullable();
 $table->Decimal('z')->nullable();
 $table->Decimal('w')->nullable();
$table->Integer('created_by')->nullable();
$table->Integer('updated_by')->nullable();
$table->Integer('is_deleted')->nullable()->default(0);
$table->datetime('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
$table->datetime('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
});
}
public function down()
{
Schema::dropIfExists('peserta');
}
}