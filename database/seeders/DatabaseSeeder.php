<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{

  public function run()
  {
    DB::table('menu')->insert([
      'url' => 'menu',
      'nama' => 'Menu',
      'tipe' => 'blank', 
      'icon' => 'fa fa-circle', 
      'color' => 'primary', 
    ]);
    DB::table('menu')->insert([
      'url' => 'dashboard',
      'nama' => 'Dashboard',
      'tipe' => 'blank', 
      'icon' => 'fas fa-columns', 
      'color' => 'primary', 
    ]);
    DB::table('menu')->insert([
      'url' => 'akses',
      'nama' => 'Akses',
      'tipe' => 'blank', 
      'icon' => 'fa fa-circle', 
      'color' => 'primary', 
    ]);
    DB::table('menu')->insert([
      'url' => 'role',
      'nama' => 'Role',
      'tipe' => 'blank', 
      'icon' => 'fa fa-circle', 
      'color' => 'primary', 
    ]);
    DB::table('menu')->insert([
      'url' => 'users',
      'nama' => 'User',
      'tipe' => 'blank', 
      'icon' => 'fa fa-circle', 
      'color' => 'primary', 
    ]);

    DB::table('role')->insert([
      'nama' => 'developer',
    ]);
    DB::table('role')->insert([
      'nama' => 'super admin',
    ]);
    DB::table('users')->insert([
      'username' => 'developer',
      'password' => bcrypt('motauaja'),
      'role_id' => 1
    ]);
    DB::table('users')->insert([
      'username' => 'admin',
      'password' => bcrypt('adminlarabon'),
      'role_id' => 2
    ]);
  

  }
}
