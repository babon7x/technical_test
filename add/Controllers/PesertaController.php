<?php

namespace Add\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
use Illuminate\Support\Facades\Auth;
use DataTables;


use Add\Requests\PesertaRequest;

use Add\Models\Peserta;

class PesertaController extends Controller
{

public function index()
{
return view('peserta.index');
}

public function list(Request $request)
{
$list=Peserta::where("is_deleted",0)->orderBy("created_at", "desc")->get();
return DataTables()->of($list)->make(true);
}

public function store(PesertaRequest $request)
{
$data = $request->all();
$data["created_by"] = Auth::id();
$store = Peserta::create($data);
return response()->json($store);
}
public function update(PesertaRequest $request)
{
$data = $request->all();
$data["updated_by"] = Auth::id();
$update = Peserta::where("id", $request->id)->update($data);
return response()->json($update);
}
public function destroy(Request $request)
{
$id = $request->ids[0];
$deleted_by = Auth::id();
$delete = Peserta::whereIn("id", request("ids"))->update(["is_deleted"=>1,"updated_by"=>$deleted_by]);
return response()->json($delete);
}
public function getData(Request $request)
{
$datas = Peserta::where("id", $request->id)->where("is_deleted",0)->get();
return response()->json($datas);
}
}