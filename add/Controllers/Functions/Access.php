<?php

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Schema;

use App\User;
use Add\Models\Menu;

function getSidebar(){
	$getSidebar 	= Storage::get('sidebar/sidebar.php');
	return $getSidebar;
}

function infoAkses()
{
	$userid   = Auth::id();
	$userinfo = User::where('id', $userid)->with('user_level')->first();
	$response['aksesuser'] = Json_encode('*');
	if ($userid > 2) {
		$response['aksesuser'] = Json_encode(Storage::get('/user/' . $userid));
	}
	$response['userinfo'] = $userinfo;

	return $response;
}

function infoMenu($nama_menu){
	$response['menu']		= Menu::where('url',$nama_menu)->first();
	$response['columns'] 	= Schema::getColumnListing($nama_menu);
	return $response;
}

