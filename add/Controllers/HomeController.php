<?php

namespace Add\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if (!session()) {

          return view('auth.login');
      }

      return view('home.home');
  }

}
