<?php

namespace Add\Requests;
use Illuminate\Foundation\Http\FormRequest;
class PesertaRequest extends FormRequest
{

public function authorize()
{
return true;
}

public function rules()
{
if($this->method() == "POST"){
return [
"nama" => "required",
"email" => "required|email",
"x" => "required",
"y" => "required",
"z" => "required",
"w" => "required",
];
}
else{
return [
"nama" => "required",
"email" => "required|email",
"x" => "required",
"y" => "required",
"z" => "required",
"w" => "required",
];
}
}

public function messages()
{
return [
"nama.required" => "tidak boleh kosong !",
"email.required" => "tidak boleh kosong !",
"email.email" => "harus berupa email valid !",
"x.required" => "tidak boleh kosong !",
"y.required" => "tidak boleh kosong !",
"z.required" => "tidak boleh kosong !",
"w.required" => "tidak boleh kosong !",
 ];
}
}