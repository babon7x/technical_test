<?php 
namespace Add\Models; 
use Illuminate\Database\Eloquent\Model; 

class Peserta extends Model 
{ 

protected $table="peserta"; 
protected $fillable=["nama","email","x","y","z","w","created_by"];

public static function getTableName() { return (new self())->getTable();} 
} 