<?php

namespace Add\Models;

use Illuminate\Database\Eloquent\Model;

class Level extends Model
{
	protected $table='level';
	protected $fillable=['nama'];

	public static function getTableName()
	{
		return (new self())->getTable();
	}
}

