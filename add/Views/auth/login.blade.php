@include('template.app-login')

<br><br>
<div class="container-fluid" >

    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    <div class="card-title">
                        <center>
                            <h4>Login</h4>
                        </center>
                    </div>

                    <br>
                    <br>


                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="form-group row undisplay">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Username') }}</label>

                            <div class="col-md-6">
                                <input id="username2" type="text" class="form-control @error('username') is-invalid @enderror" name="username2" value="{{ old('username') }}" autocomplete="username" disabled>

                                @error('username')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row undisplay">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password2" type="password" class="form-control @error('password') is-invalid @enderror" name="password2" autocomplete="current-password" disabled="">

                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>


                        <div class="form-group mb-3">
                            <div class="input-group input-group-merge input-group-alternative">
                                <div class="input-group-prepend">
                                  <span class="input-group-text text-primary"><i class="fa fa-user"></i></span>
                              </div>
                              <input id="username" type="text" class="form-control @error('username') is-invalid @enderror" name="username" value="{{ old('username') }}" required autocomplete="username" autofocus placeholder="username">


                              @error('username')
                              <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <br>


                    <div class="form-group">
                      <div class="input-group input-group-merge input-group-alternative">
                        <div class="input-group-prepend">
                          <span class="input-group-text text-primary"><i class="fa fa-lock"></i></span>
                      </div>
                      <input id="password" type="text" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="password" style="-webkit-text-security: disc;">

                      @error('password')
                      <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>

            <br><br>

            <div class="custom-control custom-control-alternative custom-checkbox undisplay">
              <input class="custom-control-input" id=" customCheckLogin" type="checkbox">
              <label class="custom-control-label" for=" customCheckLogin">
                <span class="text-muted">Remember me</span>
            </label>
        </div>
        <div class="text-center">
          <button type="submit" class="btn btn-primary my-4" style="width: 50%;">Masuk</button>
      </div>
  </form>
</div>


</div>
</div>
<div class="col-md-4"></div>
</div>
</div>