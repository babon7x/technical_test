<div class="report animated undisplay">

	<div class="row justify-content-center">
		<div class="col-12">

			<div class="card">
				<div class="card-body">

					<div class="card-title">
						<div class="row">
							<div class="col-md-12 text-right">
								<button class="btn btn-outline-danger btn-round" onclick="backToList()">back</button>
							</div>
							<div class="col-md-12">
								<h3>Laporan Nilai Peserta</h3>
								<div class="row">
									<div class="col-md-3">Nama</div>
									<div class="col-md-9"><h5 id="peserta_nama"></h5></div>
								</div>
								<div class="row">
									<div class="col-md-3">Email</div>
									<div class="col-md-9"><h5 id="peserta_email"></h5></div>
								</div>

								<hr>
								<br>

								<table class="table table-bordered" id="tableReport">	
									<thead>	
										<tr class="text-center">
											<th class="bg-primary text-white">Aspek</th>
											<th>1</th>
											<th>2</th>
											<th>3</th>
											<th>4</th>
											<th>5</th>
										</tr>
									</thead>	
									<tbody>	
										<tr id="aspekintelegensi">
											<td>Aspek Intelegensi</td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
										</tr>
										<tr id="aspeknumerical">
											<td>Aspek Numerical Ability</td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
										</tr>
									</tbody>
								</table>	

							</div>
						</div>



					</div>
				</div>
			</div>
		</div>
	</div>


</div>