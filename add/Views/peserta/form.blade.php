<div class="form animated undisplay">

	<div class="row justify-content-center">
		<div class="col-8">

			<div class="card">
				<div class="card-body">

					<div class="card-title">
						<div class="row">
							<div class="col-md-6">
								<h5>New Data Test Master</h5>
							</div>
							<div class="col-md-6 text-right">
								<button class="btn btn-outline-danger btn-round" onclick="backToList()">back</button>

							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-12">
							<input type="text" store="id" class="undisplay">
							<div class="form-group">
								<label class="control-label">nama</label>
								<input type="text" class="form-control" store="nama" value="">
							</div>
							<div class="form-group">
								<label class="control-label">email</label>
								<input type="text" class="form-control" store="email" value="">
							</div>
							<div class="form-group">
								<label class="control-label">x</label>
								<input type="text" class="form-control numberFormat" store="x" value="">
							</div>
							<div class="form-group">
								<label class="control-label">y</label>
								<input type="text" class="form-control numberFormat" store="y" value="">
							</div>
							<div class="form-group">
								<label class="control-label">z</label>
								<input type="text" class="form-control numberFormat" store="z" value="">
							</div>
							<div class="form-group">
								<label class="control-label">w</label>
								<input type="text" class="form-control numberFormat" store="w" value="">
							</div>
						</div>
					</div>

					<div class="card-foot text-right">

						<button class="btn btn-outline-success btn-round" onclick="goSave('peserta')">save</button>

					</div>

				</div>

			</div>

		</div>
	</div>

</div>
