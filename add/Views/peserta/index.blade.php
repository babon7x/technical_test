@include("template.app")

<div class="main-content container">

	<div class="card list animated fadeInDown">
		<div class="card-body">
			<div class="card-title">
				<div class="row">
					<div class="col-md-6">
						<h5>List Data Peserta</h5>
					</div>
					<div class="col-md-6 text-right">
						<button class="btn btn-primary btn-round" onclick="newData()">Tambah</button>

					</div>
				</div>
			</div>

			<table id="datatable" class="table table-sm table-bordered">
				<thead>
					<tr>
						<th rowspan="2" width="80">no</th>
						<th rowspan="2">nama</th>
						<th rowspan="2">email</th>
						<th colspan="4"><center>nilai</center></th>
						<th rowspan="2">action</th>
					</tr>
					<tr>

						<th >x</th>
						<th >y</th>
						<th >z</th>
						<th >w</th>
					</tr>
				</thead>

			</table>

		</div>
	</div>

	@include("peserta.form")
	@include("peserta.report")
	@include("peserta.xjs")

</div>