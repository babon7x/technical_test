<link href="{{asset('datatable/datatables.css')}}"></link>
<script src="{{asset('datatable/datatables.js')}}"></script>
<script src="{{asset('assets/dt_master.js')}}"></script>
<script src="{{asset('assets/data.js')}}"></script>

<script>
	$(".select2").select2({dropdownAutoWidth : true});
	$(".numberFormat").each(function() {
		new AutoNumeric(this,
		{
			watchExternalChanges: true,
			allowDecimalPadding: false,
			maximumValue:'33',
			minimumValue:'1'
		});
	});
	$(".datepicker").datepicker({
		format: "dd M yyyy"
	});

	var url = "peserta";
	var dataColum = [
	{id:null,
		"render": function ( data, type, full, meta ) {
			let baris =meta.row+1;
			let defaultContent = baris+
			'<div class="btn-group">'+
			'<div class="dropdown-menu animated bounceIn rowid" id="rowid'+full.id+'">'+
			'<a class="dropdown-item text-primary" href="#" data-id="'+full.id+'" onclick=editData(this)>Lihat/Ubah</a>'+
			'<a class="dropdown-item text-danger" href="#" data-id="'+full.id+'" onclick=deleteData(this)>Hapus</a>'+
			'</div>';
			return  defaultContent;
		}
	}];
	dataColum.push({data: 'nama', name: 'nama'})
	dataColum.push({data: 'email', name: 'email'})
	dataColum.push({id:null,'render': function ( data, type, full, meta ) {return  addCommas(full.x);} });
	dataColum.push({id:null,'render': function ( data, type, full, meta ) {return  addCommas(full.y);} });
	dataColum.push({id:null,'render': function ( data, type, full, meta ) {return  addCommas(full.z);} });
	dataColum.push({id:null,'render': function ( data, type, full, meta ) {return  addCommas(full.w);} });
	dataColum.push(
		{id:null,
			"render": function ( data, type, full, meta ) {
				let baris =meta.row+1;
				let defaultContent = 
				'<a href="#" data-id="'+full.id+'" onclick=showReport(this)>LIHAT LAPORAN</a>'+
				'|'+
				'<a href="#" data-id="'+full.id+'" onclick=editData(this)>EDIT</a>'+
				'|'+
				'<a href="#" data-id="'+full.id+'" onclick=deleteData(this)>HAPUS</a>';
				return  defaultContent;
			}
		});
	dtMaster(url,dataColum);

	$('[store="x"').on('change',function(){
		if ($(this).val()>33){
			$(this).val(33)
		}
		if ($(this).val()<1){
			$(this).val(1)
		}
	})

	$('[store="y"').on('change',function(){
		if ($(this).val()>23){
			$(this).val(23)
		}
		if ($(this).val()<1){
			$(this).val(1)
		}
	})

	$('[store="z"').on('change',function(){
		if ($(this).val()>18){
			$(this).val(18)
		}
		if ($(this).val()<1){
			$(this).val(1)
		}
	})

	$('[store="w"').on('change',function(){
		if ($(this).val()>13){
			$(this).val(13)
		}
		if ($(this).val()<1){
			$(this).val(1)
		}
	})
</script>