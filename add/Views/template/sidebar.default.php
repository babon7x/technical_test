 <div class="main-sidebar" style="z-index: 9999;">
  <aside id="sidebar-wrapper">
    <div class="sidebar-brand">
      <a href="index.html">SimpleDev</a>
    </div>
    <div class="sidebar-brand sidebar-brand-sm">
      <a href="index.html">SD</a>
    </div>

    <ul class="sidebar-menu">

      <li class="nav-item link" nama="menu">
        <a class="nav-link" href="menu">
          <i class="fa fa-circle text-primary"></i>
          <span class="nav-link-text">Menu</span>
        </a>
      </li>

      <li class="nav-item link" nama="Dashboard">
        <a class="nav-link" href="dashboard">
          <i class="fa fa-columns text-primary"></i>
          <span class="nav-link-text">Dashboard</span>
        </a>
      </li>

      <li class="nav-item" nama="Akses">
        <a href="#Akses" data-toggle="collapse" aria-expanded="false" class="nav-link dropdown-toggle collapsed">
          <i class="fa fa-edit text-primary"></i><span class="nav-link-text">Akses</span>
        </a>
        <ul class="navbar-nav collapse" id="Akses">
          <li class="nav-item link" nama="Role">
            <a class="nav-link" href="role">
              <i class="fa fa-circle text-primary"></i><span class="nav-link-text">Role</span>
            </a>
          </li>
          <li class="nav-item link" nama="User">
            <a class="nav-link" href="users">
              <i class="fa fa-circle text-primary"></i><span class="nav-link-text">User</span>
            </a>
          </li>

          <div class="dropdown-divider bg-primary"></div>
        </ul>
      </li>

    </ul>

    <div class="mt-4 mb-4 p-3 hide-sidebar-mini">
      <a href="#" class="btn btn-primary btn-lg btn-block btn-icon-split">
        <i class="fas fa-phone"></i> Contact Us
      </a>
    </div>
  </aside>
</div>