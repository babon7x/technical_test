<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>Ecommerce Dashboard &mdash; Stisla</title>

  <!-- General CSS Files -->
  <link rel="stylesheet" href="{{ asset('bootstrap/bootstrap.css') }}">
  <link rel="stylesheet" href="{{ asset('bootstrap/bootstrap-datepicker.min.css') }}">
  <link rel="stylesheet" href="{{ asset('fontawesome/fontawesome.all.css') }}">
  <link rel="stylesheet" href="{{ asset('jquery/jquery.toast.css') }}">
  <link rel="stylesheet" href="{{ asset('swal2/sweetalert2.min.css') }}">
  <link rel="stylesheet" href="{{ asset('select2/select2.min.css') }}">

  <!-- Template CSS -->
  <link rel="stylesheet" href="{{ asset('stisla/css/style.css') }}">
  <link rel="stylesheet" href="{{ asset('stisla/css/components.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/spinners.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/toast.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/animate.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/mystyle.css') }}">
</head>

<body >
  <div id="app">

    @include('template.header')
    @include('template.sidebar')

  </div>
  <!-- General JS Scripts -->
  <script src="{{ asset('jquery/jquery.min.js') }}"></script>
  <script src="{{ asset('assets/popper.js') }}"></script>
  <script src="{{ asset('bootstrap/bootstrap.bundle.js') }}"></script>
  <script src="{{ asset('bootstrap/bootstrap-notify.min.js') }}"></script>
  <script src="{{ asset('bootstrap/bootstrap-datepicker.min.js') }}"></script>

  <script src="{{ asset('jquery/jquery-nicescroll.min.js') }}"></script>
  <script src="{{ asset('jquery/jquery.toast.js') }}"></script>
  <script src="{{ asset('assets/moment.js') }}"></script>
  <script src="{{ asset('swal2/sweetalert2.all.min.js') }}"></script>
  <script src="{{ asset('select2/select2.full.min.js') }}"></script>
  <script src="{{ asset('autonumeric/AutoNumeric.js') }}"></script>

  <!-- Template JS Scripts -->
  <script src="{{ asset('stisla/js/stisla.js') }}"></script>
  <script src="{{ asset('stisla/js/scripts.js') }}"></script>

  <script src="{{ asset('assets/customnotif.js') }}"></script>
  <script src="{{ asset('assets/global.js') }}"></script>

</body>
</html>
