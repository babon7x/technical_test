<div class="main-sidebar" style="z-index: 9999;">

<aside id="sidebar-wrapper">

<div class="sidebar-brand">

<a href="index.html">PT Global Talentlytica Indonesia</a>

</div>

<div class="sidebar-brand sidebar-brand-sm">

<a href="index.html">SD</a>

</div>

<ul class="sidebar-menu">

<li class="nav-item link" nama="Dashboard">
<a class="nav-link" href="/dashboard">
<i class="fa fa-circle text-primary"></i><span class="nav-link-text">Dashboard</span>
</a>
</li>

<li class="nav-item link" nama="Peserta">
<a class="nav-link" href="/peserta">
<i class="fa fa-circle text-primary"></i><span class="nav-link-text">Peserta</span>
</a>
</li>
</ul>

<div class="mt-4 mb-4 p-3 hide-sidebar-mini">

<a href="#" class="btn btn-primary btn-lg btn-block btn-icon-split">

<i class="fas fa-phone"></i> Contact Us

</a>

</div>

</aside>

</div>