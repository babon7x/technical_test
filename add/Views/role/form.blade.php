<div class="form animated undisplay">

	<div class="row justify-content-center">
		<div class="col-9">

			<div class="card">
				<div class="card-body">

					<div class="card-title">
						<div class="row">
							<div class="col-md-6">
								<h5>New Data Role</h5>
							</div>
							<div class="col-md-6 text-right">
								<button class="btn btn-outline-danger btn-round" onclick="backToList()">back</button>

							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-12">
							<input type="text" store="id" class="undisplay">
							<div class="form-group">
								<label class="control-label">nama</label>
								<input type="text" class="form-control" store="nama" value="">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 text-right">
							<button class="btn btn-sm btn-danger mr-2" onclick="uncheckAll()">
								<i class="fa fa-times"></i>
								uncheck all
							</button>
							<button class="btn btn-sm btn-primary" onclick="checkAll()">
								<i class="fa fa-check"></i>
								check all
							</button>
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-md-12">

							<table class="table" id="form-table">
								<thead>
									<tr>
										<th width="80">no</th>
										<th>menu</th>
										<th width="80">akses</th>
										<th width="80">lihat</th>
										<th width="80">tambah</th>
										<th width="80">ubah</th>
										<th width="80">hapus</th>
										<th width="80">download</th>
									</tr>
								</thead>
								<tbody>
									@foreach($menu as $key => $value)
									<tr data-url="{{ $value->url }}">
										<td>{{ $key+1 }}</td>
										<td>
											<input type="text" class="undisplay" value="{{ $value->url }}">
											{{ $value->nama }}
										</td>
										<td class="text-center"><input type="checkbox" class="store akses"></td>
										<td class="text-center"><input type="checkbox" class="store"></td>
										<td class="text-center"><input type="checkbox" class="store"></td>
										<td class="text-center"><input type="checkbox" class="store"></td>
										<td class="text-center"><input type="checkbox" class="store"></td>
										<td class="text-center"><input type="checkbox" class="store"></td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>

					<div class="card-foot text-right">

						<button class="btn btn-outline-success btn-round" onclick="goSaveRole()">save</button>

					</div>

				</div>

			</div>

		</div>
	</div>

</div>
