<?php

use Illuminate\Support\Facades\Route;

Auth::routes();
Route::group(['middleware' => ['auth']], function() {
	Route::get('/', 'HomeController@index')->name('home');
	Route::get('/dashboard', 'HomeController@index');

//------//
	Route::resource('/role', 'RoleController');
	Route::post('/role/list', 'RoleController@list');
	Route::post('/role/getdata', 'RoleController@getData');


//------//
Route::resource('/peserta', 'PesertaController');
Route::post('/peserta/list', 'PesertaController@list');
Route::post('/peserta/getdata', 'PesertaController@getData');

});//endfile